# SC2
# Kenneth Catausan
# Shaviro on Technology and Connection

Shaviro writes regarding the age of electronic media and how society has been affected by the newfound conntectivity and possibilities.
It is an era where everyone is monitored and seen, and out of their own volition, whether they know this or not.
People in the modern age desire to find connections through the internet, and with the availability of this connective media, we are all able to view and talk to each other.
While this is supposed to be a positive thing, this very concept is made possible by certain technologies and specific businesses, such as internet service providers.
**People give their will to corporations for this desire to connect with others**, allowing any and all of their words to be archived and possibly used against them.
The world is being seen as only something to be recorded, from the visually stunning images found to the smallest and insignificant text messages. Completely severing any links in the grand network that encompasses the world is essentially **impossible**,
but what *can* be done is any *lessening* of connnectivity.
Even if massive powerful entities such as the government or corporations were not trying to monitor and save every single letter and digit sent out by human hands, there would still be many posts and sites archived, as even the *people themselves*
would go out of their way to save everything. *Anyone* for *any reason* can save *anything* made by someone else.

![YouTube's Home Page as it appeared in 2006. Note the URL, "web archive."](https://lh3.googleusercontent.com/dWRctVBhrvNGCWr5_meJ72-VoxKGLS7BYqBTePMo1b3PVc6aCeh4SrzLodVMommHc5PcROh6=w640-h400-e365)

*YouTube's Home Page as it appeared in 2006. Note the URL, "web archive."*
***
The strongest possible case of Shaviro's description of modern society's constant need to connect and televise is [**The Wayback Machine**.](https://archive.org/web/)

This is a project created and organized by a non-profit organiztion known as *Internet Archive*, dedicated to preserving biref snippets of digital history on the wrold wide web.
As it accepts any contribution for any reason, it is popular for not only capturing the nostalgic and old layouts of sites from the past, but is also used to make sure that articles and social media posts made by people *will never remain completely deleted*.

![The modern YouTube Home Page as of 2019.](https://www.youtube.com/yts/img/new_promo_page/home_screenshot_r_600-vflo6WSLb.png)

*The modern YouTube Home Page as of 2019.*
***
This archival project stands exemplary to the connecting and saving mentality of humanity int he mdoern era of technology, with all information and networking with others becoming more and more normalized.
Everyone now thinks to save any text whenever they can, when years before, many believed in simply leaving most posts alone. The willingness to connect and the idea that everything should be recorded is *deeply* rooted into people's minds.